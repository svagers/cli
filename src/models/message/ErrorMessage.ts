import { Message } from './Message';

export class ErrorMessage extends Message {
  constructor(protected message: string) {
    super('Error', message);
    this.type = 'error';
  }
}
