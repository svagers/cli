export class Message {
  protected type = 'chat';

  constructor(public author: string = 'Server', public content: string, public is_me: boolean = false) {}
}
