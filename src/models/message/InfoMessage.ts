import { Message } from './Message';

export class InfoMessage extends Message {
  constructor(protected message: string) {
    super('Info', message);
    this.type = 'info';
  }
}
