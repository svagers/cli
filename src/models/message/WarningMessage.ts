import { Message } from './Message';

export class WarningMessage extends Message {
  constructor(protected message: string) {
    super('Warning', message);
    this.type = 'warning';
  }
}
