import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/components/pages/home.vue';
import Chatroom from '@/components/pages/chatroom.vue';

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/chat',
      name: 'Chatroom',
      component: Chatroom
    }
  ]
})
